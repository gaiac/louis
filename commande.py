#!/usr/bin/env python
#auteur LMG
#license GPL V3 : https://www.gaiac.eu/gpl.txt
#ver 0.3
from Tkinter import * 
import urllib as url
import os, os.path
import tkFileDialog
import pyfirmata
import serial, time

PORT = '/dev/ttyACM0'

print (":Interface Louis 0.3")
print (":Port de communication selectionne")
print (PORT)

arduino = serial.Serial(PORT, 9600, timeout=.1)


def exec_vc_path(commande3,path):
		commande1="cd "+path
		return 'echo "' + '#/bin/bash ' + '\n' + commande1 + '\n' + commande3 + '"' + '> exec_ ; chmod u+x exec_ ; ./exec_'

def extrait_nom_fichier(nom_fichier):
		#extraction nom de fichier et extension
		#-1inverse la chaine et cherche le premier slash
		input_invers=nom_fichier[::-1]
		input_ouest_slash=input_invers.index("/")
		#1extrait le nom fichier de la chaine d'origine
		input_fichier=nom_fichier[len(nom_fichier)-input_ouest_slash:]
		#2extrait le repertoire de la chaine d'origine
		input_chemin=nom_fichier[:len(nom_fichier)-input_ouest_slash-1]
		#inverse la chaine et cherche le premier point
		input_fichier_invers=nom_fichier[::-1]
		input_ouest_extension=input_fichier_invers.index(".")
		#3extrait l'extension du nom de fichier
		input_extension=input_fichier[len(input_fichier)-input_ouest_extension:]
		#4extrait le nom fichier sans l'extension du nom de fichier
		input_fichier_sans_extension=input_fichier[0:len(input_fichier)-input_ouest_extension-1]
		#prepare et envoie lre resultat
		bilan_nom_fichier_extension=[input_chemin,input_fichier_sans_extension,input_extension]
		return bilan_nom_fichier_extension

class Application(Frame):

	def __init__(self,parent):
		Frame.__init__(self)
		self.parent = parent
		self.rep=os.getcwd()

		self.lab_iface1 = Label(self, text='Lien: ',font='Sans',underline=0)
		self.urlLien1= Entry(self,width=100)
		self.lab_iface2 = Label(self, text='Chemin: ',font='Sans',underline=0)
		self.urlLien2= Entry(self,width=100)
		self.urlButton13=Button(self,text='fichiers',font='Sans', command=self.gestfichier)
		self.urlButton14=Button(self,text='repertoire',font='Sans', command=self.gestrep)
		self.lab_iface3 = Label(self, text='Option: ',font='Sans',underline=0)
		self.urlLien3= Entry(self,width=100)
		self.quitter = Button(self, text='Quitter', font='Sans', command=self.leave)
		self.urlButton15=Button(self,text='Gauche',font='Sans', command=self.comm_a_gauche)
		self.urlButton151=Button(self,text='GaucheDroiteStop',font='Sans', command=self.comm_a_gauche_stop)
		self.urlButton16=Button(self,text='Droite',font='Sans', command=self.comm_a_droite)
		self.urlButton17=Button(self,text='Avance',font='Sans', command=self.comm_avance)
		self.urlButton171=Button(self,text='AvanceReculeStop',font='Sans', command=self.comm_avance_stop)
		self.urlButton18=Button(self,text='Recule',font='Sans', command=self.comm_recule)
		self.urlButton19=Button(self,text='Tete a gauche',font='Sans', command=self.comm_tete_a_gauche)
		self.urlButton191=Button(self,text='Tete_Stop',font='Sans', command=self.comm_tete_a_gauche_stop)
		self.urlButton20=Button(self,text='Commande manuelle',font='Sans', command=self.comm_manuelle)
		self.urlButton21=Button(self,text='Tete a droite',font='Sans', command=self.comm_tete_a_droite)
		self.urlButton22=Button(self,text='Allumer lumiere',font='Sans', command=self.comm_lumiere_allumee)
		self.urlButton221=Button(self,text='Eteindre lumiere',font='Sans', command=self.comm_lumiere_eteinte)
		self.urlButton23=Button(self,text='Rentrer bras',font='Sans', command=self.comm_rentrer_bras)
		self.urlButton231=Button(self,text='Bras Stop',font='Sans', command=self.comm_stop_bras)
		self.urlButton24=Button(self,text='Sortir bras',font='Sans', command=self.comm_sortir_bras)


		self.lab_iface1.grid(row=0,column=0,columnspan=1,sticky=NSEW)
		self.urlLien1.grid(row=0,column=1,columnspan=4,sticky=NSEW)
		self.lab_iface2.grid(row=1,column=0,columnspan=1,sticky=NSEW)
		self.urlLien2.grid(row=1,column=1,columnspan=4,sticky=NSEW)
		self.urlButton13.grid(row=1, column=4, sticky=NSEW)
		self.urlButton14.grid(row=1, column=3, sticky=NSEW)
		self.lab_iface3.grid(row=2,column=0,columnspan=1,sticky=NSEW)
		self.urlLien3.grid(row=2,column=1,columnspan=4,sticky=NSEW)
		self.quitter.grid(row=3, column=1, sticky=NSEW)
 		self.urlButton15.grid(row=7, column=1, sticky=NSEW)
 		self.urlButton151.grid(row=8, column=1, sticky=NSEW)
 		self.urlButton16.grid(row=7, column=2, sticky=NSEW)
 		self.urlButton161.grid(row=8, column=2, sticky=NSEW)
 		self.urlButton17.grid(row=7, column=3, sticky=NSEW)
 		self.urlButton171.grid(row=8, column=3, sticky=NSEW)
 		self.urlButton18.grid(row=7, column=4, sticky=NSEW)
 		self.urlButton181.grid(row=8, column=4, sticky=NSEW)
 		self.urlButton19.grid(row=7, column=5, sticky=NSEW)
 		self.urlButton191.grid(row=8, column=5, sticky=NSEW)
 		self.urlButton20.grid(row=9, column=1, sticky=NSEW)
		self.urlButton21.grid(row=9, column=2, sticky=NSEW)
		self.urlButton211.grid(row=10, column=2, sticky=NSEW)
		self.urlButton22.grid(row=9, column=3, sticky=NSEW)
		self.urlButton221.grid(row=10, column=3, sticky=NSEW)
		self.urlButton23.grid(row=9, column=3, sticky=NSEW)
		self.urlButton231.grid(row=10, column=3, sticky=NSEW)
		self.urlButton24.grid(row=9, column=3, sticky=NSEW)
		self.urlButton241.grid(row=10, column=3, sticky=NSEW)

		photo = PhotoImage(file="/home/gaiac/appli/gaail/images/gaail.ppm")
		self.urlLabellabel = Label(image=photo)
		self.urlLabellabel.image = photo # keep a reference!
		self.urlLabellabel.pack()
		self.urlLabellabel.grid(row=1,column=0,columnspan=1,sticky=NSEW)
		self.port = PORT
		self.board = pyfirmata.Arduino(self.port)


	def leave(self):
		quit()

	def gestfichier(self):
		self.repfic = tkFileDialog.askopenfilename(title="Ouvrir le fichier:", initialdir=self.rep, initialfile="", filetypes = [("All", "*"),("Fichiers Audio","*.mp3"),("Fichiers Video","*.mp4")])
		self.urlLien2.insert(0,self.repfic)

	def gestrep(self):
		self.repfic = rep = tkFileDialog.askdirectory(title="Repertoire de travail", initialdir=self.rep)
		self.urlLien2.insert(0,self.repfic)

	def comm_a_gauche(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("g")
		print (":a gauche")

	def comm_a_gauche_stop(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("G")
		print (":a gauche stop")

	def comm_a_droite(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("d")
		print (":a droite")

	def comm_a_droite_stop(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("D")
		print (":a droite stop")

	def comm_avance(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("a")
		print (":avance")

	def comm_avance_stop(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("A")
		print (":avance stop")

	def comm_recule(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("r")
		print (":recule")

	def comm_recule_stop(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("R")
		print (":recule stop")

	def comm_tete_a_gauche(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("z")
		print (":tete a gauche")

	def comm_tete_a_gauche_stop(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("Z")
		print (":tete a gauche stop")

	def comm_manuelle(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("S")
		print (":retour manuelle")

##
	def comm_tete_a_droite(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("y")
		print (":tete a droite")

	def comm_tete_a_droite_stop(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("Y")
		print (":tete a droite stop")

	def comm_lumiere_allumee(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("l")
		print (":Lumiere allumee")

	def comm_lumiere_eteinte(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("L")
		print (":Lumiere eteinte")

	def comm_rentrer_bras(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("y")
		print (":tete a droite")

	def comm_stop_bras(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("Y")
		print (":tete a droite stop")

	def comm_comm_sortir_bras(self):
		arduino = serial.Serial(PORT, 9600, timeout=.1)
		arduino.write("y")
		print (":tete a droite")

		self.urlButton23=Button(self,text='Rentrer bras',font='Sans', command=self.comm_rentrer_bras)
		self.urlButton231=Button(self,text='Bras Stop',font='Sans', command=self.comm_stop_bras)
		self.urlButton24=Button(self,text='Sortir bras',font='Sans', command=self.comm_sortir_bras)


if __name__ == '__main__':
	fen = Tk()
	fen.title('Gaail')
	fen.resizable(False,False)
 
	app = Application(fen)
	app.grid(row=0, column=0, sticky=NSEW)
 
	fen.mainloop()
